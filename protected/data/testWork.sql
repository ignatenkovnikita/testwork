-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 11 2014 г., 18:17
-- Версия сервера: 5.5.37-0ubuntu0.13.10.1
-- Версия PHP: 5.5.3-1ubuntu2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `testWork`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cinema_hal`
--

CREATE TABLE IF NOT EXISTS `cinema_hal` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '№',
  `cinema_id` int(11) NOT NULL COMMENT 'Кинотеатра',
  `number` varchar(45) NOT NULL COMMENT 'Номер зала',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `cinema_hal`
--

INSERT INTO `cinema_hal` (`id`, `cinema_id`, `number`) VALUES
(1, 1, '1'),
(2, 1, '2'),
(3, 2, '1'),
(4, 2, '2');

-- --------------------------------------------------------

--
-- Структура таблицы `film`
--

CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '№',
  `name` varchar(100) NOT NULL COMMENT 'Название',
  `description` varchar(255) DEFAULT NULL COMMENT 'Описание',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `film`
--

INSERT INTO `film` (`id`, `name`, `description`) VALUES
(1, 'Первый фильм', 'Описание первого фильма'),
(2, 'Второй фильм', 'Описание второго фильма'),
(3, 'Третий фильм', 'Описание третьего фильма'),
(4, 'Четвертый фильм', 'Описание четвертого фильма'),
(5, 'Пятый фильм', 'Описание пятого фильма');

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '№',
  `time` datetime DEFAULT NULL COMMENT 'Время сеанса',
  `cinema_hal_id` int(11) NOT NULL,
  `film_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_session_cinema_hal_idx` (`cinema_hal_id`),
  KEY `fk_session_film1_idx` (`film_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`id`, `time`, `cinema_hal_id`, `film_id`) VALUES
(1, '2014-05-10 00:00:00', 1, 1),
(2, '2014-05-09 13:33:00', 1, 1),
(3, '2014-05-01 17:44:00', 2, 1),
(4, '2014-05-16 00:40:00', 4, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Номер\n',
  `price` float DEFAULT NULL COMMENT 'Цена',
  `place` int(11) NOT NULL COMMENT 'Место',
  `description` varchar(255) DEFAULT NULL COMMENT 'Описание',
  `session_id` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ticket_session1_idx` (`session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `ticket`
--

INSERT INTO `ticket` (`id`, `price`, `place`, `description`, `session_id`, `status`, `code`) VALUES
(1, 200, 2, 'Билет второе место', 2, 1, NULL),
(2, 150, 2, NULL, 1, 0, NULL);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `fk_session_cinema_hal` FOREIGN KEY (`cinema_hal_id`) REFERENCES `cinema_hal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_session_film1` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `fk_ticket_session1` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
