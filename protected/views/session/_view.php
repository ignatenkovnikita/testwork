<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('time')); ?>:
	<?php echo GxHtml::encode($data->time); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cinema_hal_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cinemaHal)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('film_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->film)); ?>
	<br />

</div>