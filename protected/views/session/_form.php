<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'session-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

    <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',array(
            'model'=>$model, //Model object
            'attribute'=>'time', //attribute name
            'mode'=>'datetime', //use "time","date" or "datetime" (default)
            'options'=>array(
                "dateFormat"=>'yy-mm-dd',
                //'showSecond'=>true,
            )
        ));
    ?>

		<div class="row">
		<?php echo $form->labelEx($model,'time'); ?>
		<?php //echo $form->textField($model, 'time'); ?>
		<?php echo $form->error($model,'time'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cinema_hal_id'); ?>
		<?php echo $form->dropDownList($model, 'cinema_hal_id', GxHtml::listDataEx(CinemaHal::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'cinema_hal_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'film_id'); ?>
		<?php echo $form->dropDownList($model, 'film_id', GxHtml::listDataEx(Film::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'film_id'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('tickets')); ?></label>
		<?php echo $form->checkBoxList($model, 'tickets', GxHtml::encodeEx(GxHtml::listDataEx(Ticket::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->