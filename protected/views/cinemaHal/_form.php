<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'cinema-hal-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'cinema_id'); ?>
		<?php echo $form->textField($model, 'cinema_id'); ?>
		<?php echo $form->error($model,'cinema_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'number'); ?>
		<?php echo $form->textField($model, 'number', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'number'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('sessions')); ?></label>
		<?php echo $form->checkBoxList($model, 'sessions', GxHtml::encodeEx(GxHtml::listDataEx(Session::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->