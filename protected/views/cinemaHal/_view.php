<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('cinema_id')); ?>:
	<?php echo GxHtml::encode($data->cinema_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('number')); ?>:
	<?php echo GxHtml::encode($data->number); ?>
	<br />

</div>