<?php

class CinemaHalController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'CinemaHal'),
		));
	}

	public function actionCreate() {
		$model = new CinemaHal;


		if (isset($_POST['CinemaHal'])) {
			$model->setAttributes($_POST['CinemaHal']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'CinemaHal');


		if (isset($_POST['CinemaHal'])) {
			$model->setAttributes($_POST['CinemaHal']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'CinemaHal')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('CinemaHal');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new CinemaHal('search');
		$model->unsetAttributes();

		if (isset($_GET['CinemaHal']))
			$model->setAttributes($_GET['CinemaHal']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}