<?php

Yii::import('application.models._base.BaseTicket');

class Ticket extends BaseTicket
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function statusType() {
        return array(
            '0'=>'Не куплен',
            '1'=>'Куплен',
            '2'=>'Забронирован',
        );
    }

    public function getStatusName()
    {
        $r = $this->statusType();
        return $r[$this->status] ? $r[$this->status] : $this->status;
    }
}